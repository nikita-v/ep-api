import '../src/env'
import { normalizeEmail, trim } from 'validator'
import { hashSync } from 'bcryptjs'

import db from '../src/context/db'

db.mutation.createUser({
  data: {
    email: normalizeEmail(process.env.OWNER_EMAIL),
    name: trim(process.env.OWNER_NAME),
    password: hashSync('123456', 8),
    role: 'OWNER'
  }
})
