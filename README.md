# Education Platform Api

## Get started:

### 1. Clone the repo

```
git clone git@bitbucket.org:nikita-v/ep-api.git
```

### 2. Specify env variables:

You can use .env file

```
OWNER_EMAIL="vylegzhanin.nikita@gmail.com"
OWNER_NAME="Nikita Vylegzhanin"

SERVER_PORT=4000

DB_ENDPOINT="http://localhost:4466"

JWT_SECRET=“jWTs3cR3t”
JWT_EXPIRES="7d"

GMAIL_AUTH_USER="vylegzhanin.nikita@gmail.com"
GMAIL_AUTH_PASS=“…”
```

### 3. Install npm dependencies

### 4. Start docker container

### 5. Deploy Prisma Api

```
prisma deploy
```

### 6. Seed DB data

```
prisma seed
```

### 7. Install nodemon globally

```
npm install -g nodemon
```

### 8. Start dev server

```
yarn dev
```
