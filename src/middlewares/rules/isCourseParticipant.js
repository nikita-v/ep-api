import { rule } from 'graphql-shield'

export default rule()(
  async (_, { courseId, lessonId }, { user: { id }, db }) =>
    await db.exists.Course(
      lessonId
        ? {
            AND: [{ lessons_some: { id: lessonId } }, { users_some: { id } }]
          }
        : {
            AND: [{ id: courseId }, { users_some: { id } }]
          }
    )
)
