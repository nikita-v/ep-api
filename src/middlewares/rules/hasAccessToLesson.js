import { rule } from 'graphql-shield'

export default rule()(async (_, { lessonId }, { user, db }) => {
  const lessons = await db.query.lessons(
    {
      where: {
        course: {
          id: await db.query.courses(
            { where: { lessons_some: { id: lessonId } } },
            '{ id }'
          ).courseId,
          users_some: { id: user.id }
        }
      }
    },
    `{
      id
      accessRule
      completedBy(where: { id: "${user.id}" }) {
        id
      }
    }`
  )

  const lesson = lessons.find(v => v.id === lessonId)

  if (lesson.accessRule === 'INITIALLY') return true // open lesson

  if (lesson.accessRule === 'CONSISTENTLY') {
    if (lesson.completedBy.length > 0) return true // completed lesson

    if (
      lessons[lessons.findIndex(v => v.id === lessonId) - 1].completedBy
        .length > 0
    )
      return true
  }

  return false
})
