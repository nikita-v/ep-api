import { rule } from 'graphql-shield'

export default name =>
  rule(name)(async (_, args, { user: { role } }) => role === name)
