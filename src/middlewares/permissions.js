import { shield, not, and, or } from 'graphql-shield'

import {
  isAuth,
  hasRole,
  isCourseParticipant,
  hasAccessToLesson
} from './rules'

export default shield({
  Query: {
    user: isAuth,
    users: and(isAuth, isCourseParticipant),
    courses: isAuth,
    course: and(isAuth, isCourseParticipant),
    lessons: and(isAuth, isCourseParticipant),
    lesson: and(
      isAuth,
      or(hasRole('OWNER'), and(isCourseParticipant, hasAccessToLesson))
    )
  },
  Mutation: {
    login: not(isAuth),
    invite: and(isAuth, hasRole('OWNER')),
    addTask: and(isAuth, or(hasRole('OWNER'), hasRole('COACH'))),
    addQuestion: and(isAuth, or(hasRole('OWNER'), hasRole('COACH'))),
    addLesson: and(isAuth, or(hasRole('OWNER'), hasRole('COACH'))),
    removeLesson: and(isAuth, or(hasRole('OWNER'), hasRole('COACH'))),
    addCourse: and(isAuth, hasRole('OWNER')),
    editCourse: and(isAuth, hasRole('OWNER')),
    removeCourse: and(isAuth, hasRole('OWNER')),
    addAnswer: and(isAuth, or(hasRole('OWNER'), hasRole('COACH')))
  }
})
