import './env'
import { GraphQLServer } from 'graphql-yoga'

import typeDefs from './schema.graphql'
import resolvers from './resolvers'
import context from './context'
import middlewares from './middlewares'
import formatError from './formatError'

const server = new GraphQLServer({
  typeDefs,
  resolvers,
  middlewares,
  context
})

server.start({
  port: process.env.SERVER_PORT,
  formatError
})
