import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { ApolloError } from 'apollo-errors'

export default async (_, args, { select, db }) => {
  // Select arguments
  const { email, password } = select(args, {
    email: { isEmail: true },
    password: { isRequired: true }
  })

  // Find user
  const user = await db.query.user({ where: { email } })
  if (!user) throw new ApolloError({ email: ['EMAIL_NOT_FOUND'] })

  // Compare passwords
  if (!(await compare(password, user.password)))
    throw new ApolloError({ password: ['PASSWORD_WRONG'] })

  // Return AuthPayload
  return {
    token: await sign({ id: user.id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRES
    }),
    user
  }
}
