export default async (_, args, { select, user, db }, info) => {
  // Select arguments
  const { title, description } = select(args, {
    title: { min: 3, max: 64 },
    description: { max: 240 }
  })

  // Save data and return created course
  return db.mutation.createCourse(
    {
      data: {
        title,
        description,
        users: {
          connect: [{ id: user.id }]
        }
      }
    },
    info
  )
}
