export default async (_, args, { select, db }, info) => {
  // Select arguments
  const { title, description } = select(args, {
    title: { min: 3, max: 64 },
    description: { max: 240 }
  })

  // Save and return data
  return db.mutation.updateCourse(
    {
      where: {
        id: args.courseId
      },
      data: {
        title,
        description
      }
    },
    info
  )
}
