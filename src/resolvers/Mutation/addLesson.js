export default async (_, args, { select, db }, info) => {
  // Select arguments
  const { title } = select(args, {
    title: { min: 3, max: 64 }
  })

  // Save data and return created lesson
  return db.mutation.createLesson(
    {
      data: {
        title,
        text: args.text,
        accessRule: args.accessRule,
        course: {
          connect: { id: args.courseId }
        }
      }
    },
    info
  )
}
