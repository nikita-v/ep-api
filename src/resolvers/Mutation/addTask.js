export default async (_, args, { select, db }, info) => {
  // Select arguments
  const { title } = select(args, { title: { max: 64 } })

  // Save data and return created task
  return db.mutation.createTask(
    {
      data: {
        title,
        text: args.text,
        lesson: { connect: { id: args.lessonId } }
      }
    },
    info
  )
}
