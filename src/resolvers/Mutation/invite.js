import { ApolloError } from 'apollo-errors'
import { hash } from 'bcryptjs'

export default async (_, args, { select, db, sendEmail }) => {
  // Select arguments
  const { email } = select(args, { email: { isEmail: true } })

  // Find user
  let user = await db.query.user({ where: { email } })

  if (!user) {
    // Make new user if not found
    const password = Math.random()
      .toString(36)
      .substring(7)

    user = await db.mutation.createUser({
      data: {
        email,
        name: email.substring(0, email.indexOf('@')),
        password: await hash(password, 8),
        role: 'STUDENT'
      }
    })

    // Send password to the user's email
    sendEmail({
      to: user.email,
      subject: 'You been registered',
      text: `Your password is <code>${password}</code>`
    })
  }

  // Send error if user is already invited
  if (
    await db.exists.Course({ id: args.courseId, users_some: { id: user.id } })
  )
    throw new ApolloError({ email: ['USER_INVITED'] })

  // Add user to course
  await db.mutation.updateCourse({
    where: { id: args.courseId },
    data: {
      users: { connect: { id: user.id } }
    }
  })

  // Send email notification
  sendEmail({
    to: user.email,
    subject: 'You been invited',
    text: 'You have been invited to a course'
  })

  // Return the user
  return user
}
