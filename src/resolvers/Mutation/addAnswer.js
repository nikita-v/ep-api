export default async (_, args, { select, db }, info) => {
  // Select arguments
  const { text } = select(args, { text: { isRequired: true } })

  // Save data and return question
  return db.mutation.createAnswer(
    {
      data: {
        text,
        question: { connect: { id: args.questionId } },
        correct: args.correct
      }
    },
    info
  )
}
