export default async (_, { courseId }, { db }, info) =>
  db.mutation.deleteCourse({ where: { id: courseId } }, info)
