export default async (_, { lessonId }, { db }, info) =>
  db.mutation.deleteLesson({ where: { id: lessonId } }, info)
