export default async (_, args, { select, db }, info) => {
  // Select arguments
  const { text } = select(args, { text: { isRequired: true } })

  // Save data and return question
  return db.mutation.createQuestion(
    {
      data: {
        text,
        task: { connect: { id: args.taskId } }
      }
    },
    info
  )
}
