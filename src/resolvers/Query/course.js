export default (_, args, { db }, info) =>
  db.query.course({ where: { id: args.courseId } }, info)
