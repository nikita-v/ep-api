export default (_, args, { user, db }, info) =>
  db.query.courses({ where: { users_some: { id: user.id } } }, info)
