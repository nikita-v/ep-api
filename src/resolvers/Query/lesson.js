export default (_, args, { db }, info) =>
  db.query.lesson({ where: { id: args.lessonId } }, info)
