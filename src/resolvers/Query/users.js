export default async (_, { courseId }, { db }) => {
  const { users } = await db.query.course(
    {
      where: { id: courseId }
    },
    '{ users { id name role } }'
  )

  return users
}
