export default async (_, args, { user, db }) => {
  const data = await db.query.lessons(
    {
      where: {
        course: {
          id: args.courseId,
          users_some: { id: user.id }
        }
      }
    },
    `{
      id
      title
      accessRule
      completedBy(where: { id: "${user.id}" }) {
        id
      }
    }`
  )

  return data.map((lesson, i) => ({
    id: lesson.id,
    title: lesson.title,
    status: ['OWNER', 'COACH'].includes(user.role)
      ? 'OPEN' // lessons always are open for admins
      : lesson.accessRule === 'INITIALLY'
        ? lesson.completedBy.length > 0
          ? 'COMPLETE'
          : 'OPEN'
        : lesson.accessRule === 'CONSISTENTLY'
          ? lesson.completedBy.length > 0
            ? 'COMPLETE'
            : data[i - 1].completedBy.length > 0
              ? 'OPEN' // lesson is open if previous completed
              : 'CLOSE'
          : 'CLOSE' // lesson closed if something has been broken
  }))
}
