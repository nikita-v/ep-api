import getUser from './getUser'
import db from './db'
import select from './select'
import sendEmail from './sendEmail'

export default async ({ request }) => ({
  user: await getUser(request),
  db,
  select,
  sendEmail
})
