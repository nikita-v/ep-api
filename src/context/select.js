import {
  normalizeEmail,
  stripLow,
  trim,
  isLength,
  isEmail,
  isEmpty
} from 'validator'
import { ApolloError } from 'apollo-errors'

export default (raw, opts) => {
  const errors = {}
  const data = {}

  // Sanitize raw data
  Object.keys(opts).forEach(fieldName => {
    let fieldValue = typeof raw[fieldName] === 'string' ? raw[fieldName] : ''

    if (Object.keys(opts[fieldName]).includes('isEmail')) {
      fieldValue = normalizeEmail(fieldValue)
    }

    data[fieldName] = stripLow(trim(fieldValue))
  })

  // Validate sanitized data
  Object.keys(opts).forEach(fieldName =>
    Object.keys(opts[fieldName]).forEach(ruleName => {
      const ruleValue = opts[fieldName][ruleName]
      const fieldErrors = []

      if (ruleName === 'min' && !isLength(data[fieldName], { min: ruleValue }))
        fieldErrors.push('VALUE_SHORT')

      if (ruleName === 'max' && !isLength(data[fieldName], { max: ruleValue }))
        fieldErrors.push('VALUE_LONG')

      if (ruleName === 'isEmail' && !isEmail(data[fieldName]))
        fieldErrors.push('EMAIL_WRONG')

      if (ruleName === 'isRequired' && isEmpty(data[fieldName]))
        fieldErrors.push('REQUIRED')

      if (fieldErrors.length > 0) errors[fieldName] = fieldErrors
    })
  )

  // Return errors or data
  if (Object.keys(errors).length > 0) throw new ApolloError(errors)

  return data
}
