import { createTransport } from 'nodemailer'

const transporter = createTransport({
  service: 'gmail',
  auth: {
    user: process.env.GMAIL_AUTH_USER,
    pass: process.env.GMAIL_AUTH_PASS
  }
})

export default ({ to, subject, text }) =>
  transporter.sendMail({
    from: 'xnikita666x@gmail.com',
    to,
    subject,
    html: text
  })
