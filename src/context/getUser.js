import { verify } from 'jsonwebtoken'

import db from './db'

export default async ({ headers: { token } }) => {
  if (!token) return null

  try {
    const { id } = await verify(token, process.env.JWT_SECRET)

    return await db.query.user({ where: { id } }, '{ id, role }')
  } catch (error) {
    return null
  }
}
