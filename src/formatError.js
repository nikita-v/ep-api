import { isInstance, formatError } from 'apollo-errors'

export default error =>
  formatError(
    isInstance(error.originalError)
      ? typeof error.originalError.name === 'string'
        ? { message: error.originalError.name }
        : error.originalError.name
      : { message: error.message }
  )
